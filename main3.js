var Account = function(balance,currency) {
    this.balance = balance;
    this.currency = currency;
};
var person = (function () {
    var details = {
        firstName: 'Janusz',
        lastName: 'Nowobogadzki',
    };
    var calculateBalance = function () {
        var suma = 0;
        for (var i = 0; i < person.accountsList.length; i++) {
            suma += person.accountsList[i].balance;
        }
        return suma;
    }
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountsList: [],
        sayHello: function () {
            return 'Imię :' + this.firstName + 
            ', Nazwisko: ' + this.lastName + 
            ', liczba kont: ' + this.accountsList.length + 
            ', suma: ' + calculateBalance(); //spytać dlaczego jest call(this)
        },
        addAccount: function (account) {
            this.accountsList.push(account);
        }
    }
}
)();

var accountB = new Account (2, 'funty')

console.log(person.sayHello());
person.addAccount(new Account(1, 'funty'));
console.log(person.sayHello());
person.addAccount(accountB);
console.log(balance);
