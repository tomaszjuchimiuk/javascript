var  personFactory  =  function  () {
    var details = {
        firstName: 'Janusz',
        lastName: 'Nowobogadzki',
        accountsList: [
            {
                balance: 2550,
                currency: 'euro'
            },
            {
                balance: 15260,
                currency: 'dolar'
            },
            {
                balance: 1546,
                currency: 'zloty'
            }
        ]
    };
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        sayHello: function () {
            return 'Imię :' + this.firstName + ', Nazwisko: ' + this.lastName + ', liczba kont: ' + details.accountsList.length
        }
    }
};


person  =  personFactory();
console.log(person.sayHello()); 