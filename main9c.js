class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
};
class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };
    findAccount(number) {
        return this.accounts.find((object) => object.number === number);
    };
    addAccount(account) {
        this.accounts.push(account);
    };
    withdraw(number, amount) {
        return new Promise((resolve, reject) => {
            const toKonto = this.findAccount(number);
            if (amount <= 0) {
                reject('You can not withdraw minus amount');
            }
            else if (!toKonto) {
                reject('This account not exist');
            }
            else if (toKonto.balance <= amount) {
                reject('You dont have money on account');
            }
            else {
                const newBalance = toKonto.balance - amount;
                setTimeout(() => resolve(`Account number: ${toKonto.number}, Withdrawn: ${amount}, New balance: ${toKonto.balance = newBalance} `), 3000);
            }
        });
    }
};
let init = (() => {
    let cardTitle = document.querySelector("h4.card-title");
    let cardText = document.querySelector("p.card-text");
    let inputNo = document.getElementById("number");
    let inputAmount = document.getElementById("amount");
    let buttonSubmit = document.querySelector("button.btn.btn-primary");

    const Foreign = new Person("Rafał", "Rafałowski", [new Account(500, "erło", 1), new Account(150, "$", 2)]);
    cardTitle.innerHTML = `Klient ${Foreign.firstName} ${Foreign.lastName}`; //wrzutka do html

    let accountsHTML = function (nameAccount) { //funkcja tworzenia listy kont
        cardText.innerHTML = 'Accounts:'; //odświerzanie listy w html
        for (let account of nameAccount.accounts) {
            let accountList = `Number: ${account.number}, Balance: ${account.balance}, Currency: ${account.currency} `;
            cardText.innerHTML += "<p>" + accountList + "</p>";
        }
    };
    accountsHTML(Foreign);

    return {
        withdrawHTML: function () {
            let accountNumber = parseInt(document.getElementById('number').value);
            let howMuch = parseFloat(document.getElementById('amount').value);
            Foreign.withdraw(accountNumber, howMuch) //wrzuca zmienne do promisa
                .then(() => accountsHTML(Foreign)) //zgodnosc z promisem i tworzy listę pętlą accountsHTML
                .catch((err) => console.log(err));
        },
        onChanged: function () { //nie działanie Submit, gdy imput pusty.
            if (inputNo.value.length > 0 && inputAmount.value.length > 0) {
                buttonSubmit.disabled = false;
            }
            else {
                buttonSubmit.disabled = true;
            }
            //buttonSubmit.disabled = !(inputNo.value.length > 0 && inputAmount.value.length > 0)
        }
    }
})();