class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
}
class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };
    calculateBalance() {
        var sum = 0;
        for (let account of this.accounts) {
            sum += account.balance; 
        }
        return sum;
    };
    sayHello() {
        return `Imię :${this.firstName} , Nazwisko:${this.lastName}, liczba kont:${this.accounts.length}, total balance:${this.calculateBalance()}`
    };
    findAccount(number) {
        return this.accounts.find((object) => object.number === number);
    };
    filterPositiveAccounts() {
        return this.accounts.filter((elem) => {
            return elem.balance > 0
        })
    };
    addAccount(account) {
        this.accounts.push(account);
    }
    withdraw(numberKonta, amount) {
        return new Promise((resolve, reject) => {
            const toKonto = this.findAccount(numberKonta);
            if (amount <= 0) {
                reject('You can not withdraw minus amount');
            }
            else if (!toKonto) {
                reject('This account not exist');
            }
            else if (toKonto.balance <= amount) {
                reject('You dont have money on account');
            }
            else {
                const newBalance = toKonto.balance - amount;
                setTimeout(() => resolve(`Account number: ${toKonto.number}, Withdrawn: ${amount}, New balance: ${toKonto.balance = newBalance} `), 3000);
            }
        });
    }
}


const Foreign = new Person("Rafał", "Rafałowski", [new Account(500, "erlo", 1), new Account(150, "$", 2)]);
console.log(Foreign.sayHello());
Foreign.addAccount(new Account(23, "nothing", 3));
Foreign.addAccount(new Account(0, "nothing", 4));
console.log(Foreign.filterPositiveAccounts());
console.log(Foreign.accounts);
console.log(Foreign.sayHello());
Foreign.withdraw(2, 50)
    .then((success) => console.log(success))
    .catch((err) => console.log(err));
Foreign.withdraw(4, 50)
    .then((success) => console.log(success))
    .catch((err) => console.log(err));
