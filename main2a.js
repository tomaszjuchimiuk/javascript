var person = (function () {
    var details = {
        firstName: 'Janusz',
        lastName: 'Nowobogadzky',
        accountsList: [
            {
                balance: 2550,
                currency: 'erlo'
            },
            {
                balance: 15260,
                currency: 'dolar'
            },
            {
                balance: 1546,
                currency: 'zloty'
            }
        ],
    };
    var calculateBalance = function () {
        var suma = 0;
        for (var i = 0; i < details.accountsList.length; i++) {
            suma += details.accountsList[i].balance;
        }
        return suma;
    };
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountsList: details.accountsList,

        sayHello: function () {
            return 'Imię :' + this.firstName + 
            ', Nazwisko: ' + this.lastName + 
            ', liczba kont: ' + person.accountsList.length + 
            ', suma: ' + calculateBalance(); 
        },
        addAccount: function (balance, currency) {
            
            this.accountsList.push({balance: balance, currency: currency});
        }
    }
}
)();

console.log(person.sayHello());
person.addAccount(1, '');
console.log(person.sayHello());
